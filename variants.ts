type _ = unknown;
type Key = string | symbol;
type ValueOf<T> = T[keyof T];
type Proper<A extends {}, B extends Partial<A>> = keyof A extends keyof B
  ? never
  : B;
type When<A, B> = A extends never ? never : B;

type Spec = Record<Key, {} | null>;

type Variant<A extends Key, B extends PropertyKey, C extends {} | null = {}> = {
  [K in A]: B;
} &
  (C extends null ? {} : C);

type Coproduct<A extends Spec, B extends Key, C = never> = ValueOf<
  {
    [K in keyof A as K extends C ? never : K]: Variant<B, K>;
  }
>;

type Constructors<A extends Spec, B extends Key> = {
  [K in keyof A]: (
    ...args: A[K] extends null ? [] : [A[K]]
  ) => Variant<B, K, A[K]>;
};

type Guards<A extends Spec, B extends Key> = {
  [K in keyof A]: (
    variants: Coproduct<A, B>
  ) => variants is Variant<B, K, A[K]>;
};

type Cases<A extends Spec, B extends Key> = {
  [K in keyof A]: (variant: Variant<B, K, A[K]>) => _;
};

interface Matching<A extends Spec, B extends Key> {
  <C extends Cases<A, B>>(cases: C): <D extends Key, E extends Variant<B, D>>(
    variant: E
  ) => ReturnType<C[E[B]]>;

  <C extends Cases<A, B>, D extends Partial<C>, E>(
    cases: D,
    otherwise: (variants: Coproduct<A, B, keyof D>) => E
  ): When<
    Proper<C, D>,
    <F extends Key, G extends Variant<B, F>>(
      component: G
    ) => F extends keyof D ? D[G[B]] : E
  >;
}

interface Variants<A extends Spec, B extends Key> {
  of: Constructors<A, B>;
  is: Guards<A, B>;
  match: Matching<A, B>;
}

const dispatch = <A>(proc: (key: Key) => _): A =>
  new Proxy({}, { get: (_: {}, key: Key) => proc(key) }) as A;

const of = <A extends Spec, B extends Key = 'tag'>(
  discriminator?: B
): Variants<A, B> => {
  const tag: Key = discriminator ?? 'tag';

  return {
    of: dispatch(key => props => ({
      [tag]: key,
      ...props,
    })),
    is: dispatch(key => variants => variants[tag] === key),
    match: ((...[cases, otherwise]) =>
      variants => {
        const proc = cases[variants[tag]] ?? otherwise;
        return proc(variants);
      }) as Matching<A, B>,
  };
};

type SomeSpec = { success: { data: number }; fault: null };

const Type = of<SomeSpec>();
const Type2 = of<SomeSpec, 'type'>('type');
const {
  of: { success, fault },
} = of<SomeSpec>();

const success1 = Type.of.success({ data: 42 });
const fault2 = Type.of.fault();

const result3 = Type2.of.success({ data: 42 });
const result4 = Type2.of.fault();

const result5 = success({ data: 9001 });
const result6 = fault();

// won't compile
// const result7 = Type.nonExistent();
// const result8 = fault({ prop: 42 });
// const result9 = success();

console.log(success1, fault2, result3, result4, result5, result6);

console.log(Type.is.fault(success1));
console.log(Type.is.success(success1));
console.log(Type.is.fault(fault2));
console.log(Type.is.success(fault2));

const T1 = Type.match({
  success: x => console.log(x),
  fault: _ => 42,
})(fault2);

const T2 = Type.match({
  success: x => console.log(x),
  fault: _ => 42,
})(success1);

const T3 = Type.match(
  {
    success: x => console.log(x),
  },
  _ => 42 as const
)(fault2);

const T4 = Type.match(
  {
    success: x => console.log(x),
  },
  x => x.tag
)(success1);

// Won't compile
// Type.match(
//     {
//         success: x => console.log(x),
//     },
// )(result2);
// Type.match(
//     {
//         fault: x => console.log(x),
//     },
// )(fault2);
// Type.match(
//   {
//     success: x => console.log(x),
//     fault: _ => 42,
//   },
//   x => x
// )(success1);
