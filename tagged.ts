type Variants = Record<PropertyKey, {}>;

type Constructors<A extends Variants, B extends PropertyKey> = {
  [K in keyof A]: keyof A[K] extends []
    ? () => Record<B, K>
    : (props: A[K]) => Record<B, K> & A[K];
};

const proxy = <A>(f: (key: PropertyKey) => {}): A =>
  new Proxy({}, { get: (_: {}, key: PropertyKey) => f(key) }) as A;

const Tagged = <A extends Variants, B extends string = 'tag'>(
  discriminator?: B
): Constructors<A, B> =>
  proxy((key: PropertyKey) => props => ({
    [discriminator ?? 'tag']: key,
    ...props,
  }));

type Spec = { success: { data: number }; fault: {} };

const Cons = Tagged<Spec>();
const Cons2 = Tagged<Spec, 'type'>('type');
const { success, fault } = Tagged<Spec>();

const result1 = Cons.success({ data: 42 });
const result2 = Cons.fault();

const result3 = Cons2.success({ data: 42 });
const result4 = Cons2.fault();

const result5 = success({ data: 9001 });
const result6 = fault();

// won't compile
const result7 = Cons.nonExistent();
const result8 = fault({ prop: 42 });
const result9 = success();

console.log(result1, result2, result3, result4);
